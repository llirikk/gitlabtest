import React from 'react'
import ReactDOM from 'react-dom'
import ModalVideo from 'react-modal-video'


class App extends React.Component {

  constructor () {
    super()
    this.state = {
      isOpen: false,
      currentVideoId : 'J6WlEAlo0uY'
      

    }
    this.openModal = this.openModal.bind(this)
    this.updateVideoId = this.updateVideoId.bind(this)
    this.updateVideoId2 = this.updateVideoId2.bind(this)
    this.updateVideoId3 = this.updateVideoId3.bind(this)
    this.updateVideoId4 = this.updateVideoId4.bind(this)
    this.updateVideoId5 = this.updateVideoId5.bind(this)
  }

  // let movies = {
  //   {videoId:'J6WlEAlo0uY', title:'Video1'},
  //   {videoId:'8Ov2MAePN18', title:'Video2'},
  //   {videoId:'8Ov2MAePN18', title:'Video3'},
  //   {videoId:'Pc6nyq9DSqs', title:'Video4'},
  //   {videoId:'PRiCxT8QGIA', title:'Video5'},
  // }

    // let movies = [
    //   videoId:'J6WlEAlo0uY',
    //   videoId:'8Ov2MAePN18',
    //   videoId:'8Ov2MAePN18',
    //   videoId:'Pc6nyq9DSqs',
    //   videoId:'PRiCxT8QGIA'
    // ]
  

  openModal () {
    this.setState({isOpen: true})
  }

  updateVideoId(currentVideoId){
    currentVideoId = 'J6WlEAlo0uY'
    this.setState({currentVideoId}) 
  }

  updateVideoId2(currentVideoId){
    currentVideoId = '8Ov2MAePN18'
    this.setState({currentVideoId}) 
  }

  updateVideoId3(currentVideoId){
    currentVideoId = '8Ov2MAePN18'
    this.setState({currentVideoId}) 
  }

  updateVideoId4(currentVideoId){
    currentVideoId = 'Pc6nyq9DSqs'
    this.setState({currentVideoId}) 
  }

  updateVideoId5(currentVideoId){
    currentVideoId = 'PRiCxT8QGIA'
    this.setState({currentVideoId}) 
  }

  render () {
    return (
      <div>
        <ModalVideo channel='youtube' isOpen={this.state.isOpen} videoId={this.state.currentVideoId} onClose={() => this.setState({isOpen: false})} />
        <button onClick={this.openModal}>Open</button>
        <div>
          <button onClick={this.updateVideoId}>Video 1</button>
          <button onClick={this.updateVideoId2}>Video 2</button>
          <button onClick={this.updateVideoId3}>Video 3</button>
          <button onClick={this.updateVideoId4}>Video 4</button>
          <button onClick={this.updateVideoId5}>Video 5</button>
        </div>

        {/* <div>
          movies.map(movie) => (<button onClick={this.updateVideoId(movie.videoId)}>{movie.title}</button>))
        </div> 
        */}
        
        {/* <div>
          [videoId:'J6WlEAlo0uY',
          videoId:'8Ov2MAePN18',
          videoId:'8Ov2MAePN18',
          videoId:'Pc6nyq9DSqs',
          videoId:'PRiCxT8QGIA'].map(m => <button onClick={() => this.updateVideoId(m.videoId)>'title'</button>)
        </div> */}
      </div> 
    )
  }
}

export default App;