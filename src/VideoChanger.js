const sources = {
    sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
    bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
    bunnyMovie: 'http://media.w3.org/2010/05/bunny/movie.mp4',
    test: 'http://media.w3.org/2010/05/video/movie_300.webm'
  };

componentDidMount() {
    // subscribe state change
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
  }
  
  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime
    });
  }

changeSource(name) {
    return () => {
      this.setState({
        source: sources[name]
      });
      this.refs.player.load();
    };
  }


<div className="pb-3">
    <Button onClick={this.changeSource('sintelTrailer')} className="mr-3">
        Sintel teaser
    </Button>
    <Button onClick={this.changeSource('bunnyTrailer')} className="mr-3">
        Bunny trailer
          </Button>
    <Button onClick={this.changeSource('bunnyMovie')} className="mr-3">
        Bunny movie
    </Button>
    <Button onClick={this.changeSource('test')} className="mr-3">
        Test movie
    </Button>
</div>